package app.fchimal.inventariomedicamentos;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.ref.WeakReference;

public class DetalleMedicina extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {
    // crear variables por conveniencia privadas
    private TextView nombreTextView;
    private TextView sustanciaTextView;
    private TextView presentacionTextView;
    private TextView laboratorioTextView;
    private TextView proveedorTextView;
    private long medicinaId;

    public static final String EXTRA_MEDICINA_ID="medicina.id.extra";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_medicina);

        nombreTextView = (TextView) findViewById(R.id.nombre_text_view);
        sustanciaTextView=(TextView) findViewById(R.id.sustancia_text_view);
        presentacionTextView=(TextView) findViewById(R.id.presentacion_text_view);
        laboratorioTextView=(TextView) findViewById(R.id.laboratorio_text_view);
        proveedorTextView=(TextView) findViewById(R.id.proveedor_text_view);

        Intent intencion = getIntent();

        medicinaId=intencion.getLongExtra(MainActivity.EXTRA_ID_MEDICINA,-1L);

        getSupportLoaderManager().initLoader(0,null,this);

        findViewById(R.id.boton_eliminar).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new DeleteMedicinaTask(DetalleMedicina.this,medicinaId).execute();
                    }
                }
        );

        findViewById(R.id.boton_actualizar).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(DetalleMedicina.this,AgregarMedicina.class);
                        intent.putExtra(EXTRA_MEDICINA_ID,medicinaId);
                        startActivity(intent);
                    }
                }
        );

    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new MedicinaLoader(this,medicinaId);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

        if(data!=null && data.moveToFirst()){
            int nombreIndex=data.getColumnIndexOrThrow(MedicinaDatabase.COL_NOMBRE);
            String nombreMedicina=data.getString(nombreIndex);

            int sustanciaIndex=data.getColumnIndexOrThrow(MedicinaDatabase.COL_SUSTANCIA);
            String sustanciaMedicina=data.getString(sustanciaIndex);

            int presentacionIndex=data.getColumnIndexOrThrow(MedicinaDatabase.COL_PRESENTACION);
            String presentacionMedicina=data.getString(presentacionIndex);

            int laboratorioIndex=data.getColumnIndexOrThrow(MedicinaDatabase.COL_lABORATORIO);
            String laboratorioMedicina=data.getString(laboratorioIndex);

            int proveedorIndex=data.getColumnIndexOrThrow(MedicinaDatabase.COL_PROVEEDOR);
            String proveedoresMedicina=data.getString(proveedorIndex);

            nombreTextView.setText(nombreMedicina);
            sustanciaTextView.setText(sustanciaMedicina);
            presentacionTextView.setText(presentacionMedicina);
            laboratorioTextView.setText(laboratorioMedicina);
            proveedorTextView.setText(proveedoresMedicina);

        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    public static class DeleteMedicinaTask extends AsyncTask<Void, Void, Boolean> {

        private WeakReference<Activity> weakActivity;
        private Long medicinaId;

        public DeleteMedicinaTask(Activity activity, long id){
            weakActivity = new WeakReference<Activity>(activity);
            medicinaId=id;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            Context context = weakActivity.get();
            if (context == null){
                return false;
            }

            Context appContext = context.getApplicationContext();

            int filasAfectadas = MedicinaDatabase.eliminaConId(appContext, medicinaId);
            return  (filasAfectadas != 0);
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            Activity context = weakActivity.get();
            if(context == null){
                return;
            }
            if (aBoolean){
                Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(context, "Failure", Toast.LENGTH_SHORT).show();
            }

            context.finish();
        }
    }

}
