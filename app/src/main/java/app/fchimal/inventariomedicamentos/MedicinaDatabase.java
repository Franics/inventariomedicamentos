package app.fchimal.inventariomedicamentos;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.support.v4.content.LocalBroadcastManager;
/**
 * Created by Conalep Maestro on 17/11/2016.
 */

public class MedicinaDatabase extends SQLiteOpenHelper{

    private static final int DATABASE_VERSION=1;
    private static final String DATABASE_NAME="medicina.db";
    private static final String TABLE_NAME="medicina";
    public  static final String COL_NOMBRE="nombre";
    public  static final String COL_SUSTANCIA="sustancia";
    public  static final String COL_PRESENTACION="presentacion";
    public  static final String COL_lABORATORIO="laboratorio";
    public  static final String COL_PROVEEDOR="proveedor";


    public MedicinaDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createQuery = "CREATE TABLE " + TABLE_NAME +
                " (_id INTEGER PRIMARY KEY, "
                + COL_NOMBRE+" TEXT NOT NULL COLLATE UNICODE, "
                + COL_SUSTANCIA+" TEXT NOT NULL, "
                + COL_PRESENTACION+" TEXT NOT NULL, "
                + COL_lABORATORIO+" TEXT NOT NULL, "
                + COL_PROVEEDOR+" TEXT NOT NULL)";

        db.execSQL(createQuery);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String upgradeQuery = "DROP TABLE IF EXISTS "+TABLE_NAME;
        db.execSQL(upgradeQuery);
    }
    public static long insertMedicina(Context context, String nombre, String sustancia, String presentacion, String laboratorio, String proveedor){
        SQLiteOpenHelper dbOpenHelper = new MedicinaDatabase(context);
        SQLiteDatabase database= dbOpenHelper.getWritableDatabase();
        ContentValues valorMedicina= new ContentValues();
        valorMedicina.put(COL_NOMBRE,nombre);
        valorMedicina.put(COL_SUSTANCIA,sustancia);
        valorMedicina.put(COL_PRESENTACION,presentacion);
        valorMedicina.put(COL_lABORATORIO,laboratorio);
        valorMedicina.put(COL_PROVEEDOR,proveedor);

        long result=-1;

        try {

            result= database.insert(TABLE_NAME,null,valorMedicina);
            if (result != -1L){
                LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(context);
                Intent intentFilter = new Intent(MedicinasLoader.ACTION_RELOAD_TABLE);
                broadcastManager.sendBroadcast(intentFilter);
            }

        } finally {
            dbOpenHelper.close();
        }


//        long result = database.insert(TABLE_NAME,null,valorAnimal);
//        dbOpenHelper.close();
        return result;


    }

    public static Cursor devuelveTodos (Context context){
        SQLiteOpenHelper dbOpenHelper = new MedicinaDatabase(context);
        SQLiteDatabase database= dbOpenHelper.getReadableDatabase();

        return database.query(
                TABLE_NAME, new String[]{COL_NOMBRE,COL_SUSTANCIA, BaseColumns._ID}
                ,null,null,null,null,
                COL_NOMBRE+" ASC");


    }

    public static Cursor devuelveConId(Context context, long identificador){
        SQLiteOpenHelper dbOpenHelper = new MedicinaDatabase(context);
        SQLiteDatabase database= dbOpenHelper.getReadableDatabase();


        return database.query(
                TABLE_NAME, new String[]{COL_NOMBRE,COL_SUSTANCIA,COL_PRESENTACION,COL_lABORATORIO,COL_PROVEEDOR,BaseColumns._ID},
                BaseColumns._ID + " = ?",
                new String[]{String.valueOf(identificador)},
                null,
                null,
                COL_SUSTANCIA+" ASC");

    }

    public static int eliminaConId(Context context, long medicinaId){
        SQLiteOpenHelper dbOpenHelper = new MedicinaDatabase(context);
        SQLiteDatabase database = dbOpenHelper.getWritableDatabase();

        int resultado = database.delete(
                TABLE_NAME,BaseColumns._ID + " =?",
                new String[]{String.valueOf(medicinaId)});

        if (resultado != 0){
            LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(context);
            Intent intentFilter = new Intent(MedicinasLoader.ACTION_RELOAD_TABLE);
            broadcastManager.sendBroadcast(intentFilter);
        }
        dbOpenHelper.close();
        return resultado;
    }

    public static int actualizaMedicina(Context context, String nombre, String sustancia, String presentacion, String laboratorio, String proveedor, long medicinaId){
        SQLiteOpenHelper dbOpenHelper = new MedicinaDatabase(context);
        SQLiteDatabase database = dbOpenHelper.getWritableDatabase();

        ContentValues valorMedicina= new ContentValues();
        valorMedicina.put(COL_NOMBRE, nombre);
        valorMedicina.put(COL_SUSTANCIA, sustancia);
        valorMedicina.put(COL_PRESENTACION, presentacion);
        valorMedicina.put(COL_lABORATORIO, laboratorio);
        valorMedicina.put(COL_PROVEEDOR, proveedor);


        int result = database.update(
                TABLE_NAME,
                valorMedicina, BaseColumns._ID + " =?",
                new String[]{String.valueOf(medicinaId)});

        if (result != 0){
            LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(context);
            Intent intentFilter = new Intent(MedicinasLoader.ACTION_RELOAD_TABLE);
            broadcastManager.sendBroadcast(intentFilter);
        }
        dbOpenHelper.close();
        return result;
    }





}

