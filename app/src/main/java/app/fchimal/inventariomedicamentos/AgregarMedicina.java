package app.fchimal.inventariomedicamentos;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.lang.ref.WeakReference;

public class AgregarMedicina extends AppCompatActivity implements View.OnClickListener, LoaderManager.LoaderCallbacks<Cursor> {

    private long medicinaId;
    private EditText nombreEditText;
    private EditText sustanciaEditText;
    private EditText presentacionEditText;
    private EditText laboratorioEditText;
    private EditText proveedorEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_medicina);

        nombreEditText =(EditText) findViewById(R.id.nombre_edit_text);
        sustanciaEditText =(EditText) findViewById(R.id.sustancia_edit_text);
        presentacionEditText =(EditText) findViewById(R.id.presentacion_edit_text);
        laboratorioEditText =(EditText) findViewById(R.id.laboratorio_edit_text);
        proveedorEditText =(EditText) findViewById(R.id.proveedor_edit_text);

        medicinaId=getIntent().getLongExtra(DetalleMedicina.EXTRA_MEDICINA_ID,-1L);
        if (medicinaId != -1L){
            getSupportLoaderManager().initLoader(0,null,this);
        }


        findViewById(R.id.boton_agregar).setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        String nombreMedicina = nombreEditText.getText().toString();
        String sustanciaMedicina = sustanciaEditText.getText().toString();
        String presentacionMedicina = presentacionEditText.getText().toString();
        String laboratorioMedicina = laboratorioEditText.getText().toString();
        String proveedorMedicina = proveedorEditText.getText().toString();

        // llamamos al metodo que crea el animal y le pasamos los parametros
        new CreateMedicinaTask(this,nombreMedicina,sustanciaMedicina,presentacionMedicina,laboratorioMedicina,proveedorMedicina, medicinaId).execute();


    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new MedicinaLoader(this,medicinaId);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

        if(data!=null && data.moveToFirst()){
            int nombreIndex=data.getColumnIndexOrThrow(MedicinaDatabase.COL_NOMBRE);
            String nombreMedicina=data.getString(nombreIndex);

            int sustanciaIndex=data.getColumnIndexOrThrow(MedicinaDatabase.COL_SUSTANCIA);
            String sustanciaMedicina=data.getString(sustanciaIndex);

            int presentacionIndex=data.getColumnIndexOrThrow(MedicinaDatabase.COL_PRESENTACION);
            String  presentacionMedicina=data.getString(presentacionIndex);

            int laboratorioIndex=data.getColumnIndexOrThrow(MedicinaDatabase.COL_lABORATORIO);
            String laboratorioMedicina=data.getString(laboratorioIndex);

            int proveedorIndex=data.getColumnIndexOrThrow(MedicinaDatabase.COL_PROVEEDOR);
            String proveedorMedicina=data.getString(proveedorIndex);

            nombreEditText.setText(nombreMedicina);
            sustanciaEditText.setText(sustanciaMedicina);
            presentacionEditText.setText(presentacionMedicina);
            laboratorioEditText.setText(laboratorioMedicina);
            proveedorEditText.setText(proveedorMedicina);

        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
    public static class CreateMedicinaTask extends AsyncTask<Void, Void, Boolean> {

        private WeakReference<Activity> weakActivity;
        private String medicinaNombre;
        private String medicinaSustancia;
        private String medicinaPresentacion;
        private String medicinaLaboratorio;
        private String medicinaProveedor;

        private long medicinaId;


        public CreateMedicinaTask(Activity activity, String nombre, String sustancia, String presentacion, String laboratorio, String proveedor,  long medicinaId){
            weakActivity = new WeakReference<Activity>(activity);
            medicinaNombre = nombre;
            medicinaSustancia = sustancia;
            medicinaPresentacion = presentacion;
            medicinaLaboratorio = laboratorio;
            medicinaProveedor = proveedor;
            this.medicinaId = medicinaId;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            Context context = weakActivity.get();
            if (context == null){
                return false;
            }

            Context appContext = context.getApplicationContext();
            boolean succes = false;

            if (medicinaId!=-1L){
                int filasAfectadas=MedicinaDatabase.actualizaMedicina(appContext, medicinaNombre, medicinaSustancia, medicinaPresentacion, medicinaLaboratorio, medicinaProveedor,medicinaId);
                succes=(filasAfectadas != 0);
            } else {
                long id=MedicinaDatabase.insertMedicina(appContext, medicinaNombre, medicinaSustancia, medicinaPresentacion, medicinaLaboratorio, medicinaProveedor);
                succes=(id != -1L);
            }

            return  succes;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            Activity context = weakActivity.get();
            if(context == null){
                return;
            }
            if (aBoolean){
                Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(context, "Failure", Toast.LENGTH_SHORT).show();
            }

            context.finish();
        }
    }

}
