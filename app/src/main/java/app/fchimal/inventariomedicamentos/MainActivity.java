package app.fchimal.inventariomedicamentos;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.database.Cursor;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

    public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener, LoaderManager.LoaderCallbacks<Cursor> {
        public final static String EXTRA_ID_MEDICINA = "MEDICINA.ID_MEDICINA";
        private SimpleCursorAdapter adaptador;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView listaDeMedicina = (ListView) findViewById(R.id.activity_main);
        listaDeMedicina.setOnItemClickListener(this);

        adaptador = new SimpleCursorAdapter(
                this,
                android.R.layout.simple_list_item_1,
                null,
                new String[]{MedicinaDatabase.COL_NOMBRE},
                new int[]{android.R.id.text1},
                0);

        listaDeMedicina.setAdapter(adaptador);

        getSupportLoaderManager().initLoader(0,null,this);


        findViewById(R.id.boton_nuevo_medicamento).setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                Intent intento = new Intent(MainActivity.this, AgregarMedicina.class);
                startActivity(intento);
            }
        });

    }

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//        String nombreAnimal= (String) parent.getItemAtPosition(position);
//        String descripcionAnimal= arregloDeDescripciones[position];

            Intent intencion = new Intent(this,DetalleMedicina.class) ; // iniciar la actividad
            intencion.putExtra(EXTRA_ID_MEDICINA,adaptador.getItemId(position));
            startActivity(intencion);
        }

        @Override
        public Loader<Cursor> onCreateLoader(int id, Bundle args) {
            return new MedicinasLoader(this);
        }

        @Override
        public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
            adaptador.swapCursor(data);
        }

        @Override
        public void onLoaderReset(Loader<Cursor> loader) {
            adaptador.swapCursor(null);
        }
    }
