package app.fchimal.inventariomedicamentos;
import android.app.Application;

import com.facebook.stetho.Stetho;
/**
 * Created by Conalep Maestro on 19/11/2016.
 */

public class MiAplicacion extends Application {
    public void onCreate(){
        super.onCreate();
        Stetho.initializeWithDefaults(this);
    }
}
